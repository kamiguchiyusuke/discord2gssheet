import gspread
from oauth2client.service_account import ServiceAccountCredentials

scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
credentials = ServiceAccountCredentials.from_json_keyfile_name('credentials.json', scope)

# 認証情報設定
gc = gspread.authorize(credentials)
spreadsheet_key = 'key'
book = gc.open_by_key(spreadsheet_key)
worksheet = book.worksheet("シート1")

worksheet.update_acell('A1', 'Hello0')
worksheet.update_acell('B1', 'World')

last_row = len(worksheet.get_all_values())

row = ["Hello1", "World"]
worksheet.insert_row(row, last_row + 1)
worksheet.append_row(row)

row = ["Hello2", "World"]
worksheet.append_row(row)
worksheet.append_row(row)
