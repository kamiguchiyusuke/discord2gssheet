#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging
import os
import queue
import asyncio
import re
import json
import discord
import gspread
import configparser

from datetime import datetime, timedelta
from discord.ext import commands
from google.oauth2.service_account import Credentials


# config.iniファイル読み込み
config = configparser.ConfigParser()
config_file = '/configfile_path/config.ini'
config.read(config_file)

# Discord設定
DISCORD_TOKEN = 'discord-bot-token'
bot = commands.Bot(command_prefix='!')

# Google Sheets API設定
scope = ['https://www.googleapis.com/auth/spreadsheets', 'https://www.googleapis.com/auth/drive']
jsonfile_path = '/path/jsonfile.json'
credentials = Credentials.from_service_account_file(jsonfile_path, scopes=scope)
gc = gspread.authorize(credentials)

# 新しいQueueを作成します
q = queue.Queue()

# グローバル変数
gsheet_key = ""
environment = "PRODUCTION"


def check_spreadsheet_exists(spreadsheet_id):
    #=========================================
    # Googleスプレッドシートがあるかを確認
    #=========================================
    try:
        # スプレッドシートを開きます。開けたらTrueを返します。
        gc.open_by_key(spreadsheet_id)
        return True
    except gspread.SpreadsheetNotFound:
        # スプレッドシートを開けない場合、Falseを返します。
        return False

def create_sheet_and_add_config(sheet_name, config_key_name):
    #================================================================================
    # Googlespreadシートを新規作成し、config.iniにスプレッドシートIDを追記します。
    #================================================================================
    global gsheet_key
    sh = gc.create(sheet_name)
    sh.share('user@gmail.com', perm_type='user', role='writer')
    print(f"新規に作成したスプレッドシートのIDは {sh.id} です")
    print(f"スプレッドシートのURLは https://docs.google.com/spreadsheets/d/{sh.id}/edit です")
    config[environment][config_key_name] = '{"sheet_name": "'+ sheet_name +'", "gsheet_key": "'  + sh.id +'"}'
    gsheet_key = sh.id
    with open(config_file, 'w') as configfile:
        config.write(configfile)

dt_now = datetime.now()                # dt_now :  2023-06-18 00:42:07.054267
dt_next = dt_now + timedelta(days=1)   # 次の日の日付を計算
dt_before = dt_now - timedelta(days=1) # 前日の日付を計算
now_day_hour   = dt_now.hour           # 現在の時間 2023-06-18 00:42:07.054267 → 0
now_day_date   = dt_now.day            # 現在の日にち  5/30 → 30
now_day_month  = dt_now.month          # 現在の日の月
next_day_date  = dt_next.day           # 次の日にち 5/30 → 6/1 → 1
next_day_month = dt_next.month         # 次の日の月
before_day_month = dt_before.month     # 前日の月


today = datetime.today()
print("現在時間 : ", today)
if now_day_hour != 23:
    today = today - timedelta(days=1) # 1日前の日付を計算
# Googlespreadシートのtab名を定義する
sheet_tab_name = today.strftime("%m/%d").replace('/0', '/').lstrip('0')

if (now_day_hour==23 and next_day_date==2) or (now_day_date==2 and now_day_hour==0):
    sheet_name = "botテスト" + str(now_day_month) + "月"
    config_key_name = "sheet_name" + str(now_day_month)
elif (now_day_hour==23 and next_day_date!=2) or (now_day_date!=2 and now_day_hour==0):
    sheet_name = "botテスト" + str(before_day_month) + "月"
    config_key_name = "sheet_name" + str(before_day_month)
else:
    sheet_name = "botテスト"
    config_key_name = "sheet_test"

if config_key_name in config[environment]: # config.iniにconfig_key_nameが記載されているかを確認
    spreadsheet_key = config[environment][config_key_name]
    config_data = json.loads(spreadsheet_key)
    gsheet_key = config_data["gsheet_key"]
    # googlespreadシートkeyがあるが、googleスプレッドシートが存在しない場合、新たにスプレッドシートを作成する
    if not check_spreadsheet_exists(gsheet_key):
        print("Google spread sheet not found. but config parameter exists")
        create_sheet_and_add_config(sheet_name, config_key_name)
else:
    create_sheet_and_add_config(sheet_name, config_key_name)

book = gc.open_by_key(gsheet_key)

# Google スプレッドシートのタブの作成
try:
    worksheet = book.worksheet(sheet_tab_name)
    worksheet.update('A1', 'MOSA Challeng Cup 2023')
    date_str = today.strftime("%Y/%-m/%-d")
    worksheet.update('D1', date_str)

    # 特定のセルの値を取得（ここでは 'D1' を指定）
    d1_cell_data = worksheet.acell('D1').value
    print("D1のデータ : ", d1_cell_data)
    print(f'Sheet "{sheet_tab_name}" already exists.')
except gspread.exceptions.WorksheetNotFound:
    # タブが存在しない場合、新しいタブを作成
    worksheet = book.add_worksheet(title=sheet_tab_name, rows="100", cols="20")
    worksheet.update('A1', 'MOSA Challeng Cup 2023')
    date_str = today.strftime("%Y/%-m/%-d")
    worksheet.update('D1', date_str)
    headers = ["名前(リンク先Twitter)", "運用開始額(除ﾎﾞｰﾅｽ)", "初期ボーナス", "現在の残高", "有効証拠金", "含み損益",
           "含み損益(%)", "収益額（円）", "収益率（％）", "総収益", "総収益率(%)", "最大含み損", "最大含み（％）",
           "追加入金", "（本日）", "出金", "（本日）", "クレジット", "通貨単位"]
    worksheet.insert_row(headers, 2)
    print(f'Sheet "{sheet_tab_name}" created.')


@bot.event
async def on_ready():
    print('We have logged in as {0.user}'.format(bot))


write_counter = 0
@bot.event
async def on_message(message):
    global write_counter
    message_content = message.content

    def extract_number(string):
        # ==============================
        # 文字列から数値を抜き出す関数
        # ==============================

        # 正規表現パターンを定義します。小数点とマイナスを含む数字を探します。
        pattern = r"(-?\d+\.\d+|-?\d+)"
        result = re.findall(pattern, string)

        # 先頭の一致（もしあれば）を返します。
        # もし result リストが空でなければ（つまり一つ以上の数値が見つかったなら）"
        if result:
            # 小数点がある場合はfloatに変換し、なければintに変換します。
            return float(result[0]) if '.' in result[0] else int(result[0])

        return string

    # メッセージを受信する特定のチャンネルIDのリスト
    channel_ids = [1110223784809595060, 1110224048790704190, 1110232912353312860, 1110232973552398436, 1110233216117383269, 1111619849995685899, 1111619928534028288, 1111620556962402345, 1112243070780579930, 1110224228873158726, 1110224698157043893, 1114595879190544495]

    # メッセージが指定のチャンネルからのものであるかをチェック
    if message.channel.id not in channel_ids:
        print("Discord channel ID : ", message.channel.id)
        return

    modified_content = message_content.replace('\r', '\n') 
    # 行数が0行以下のとき処理しない
    count_lines = modified_content.count('\n') + 1
    if count_lines <= 0:
        return

    currency = ""
    gssheet_data_list = []
    lines = message_content.split('\r')
    for line in lines:
        if line.startswith('参加名：'):
            participant_name = line[len('参加名：'):].strip()
            gssheet_data_list.append(participant_name)
        if line.startswith('運用開始額(ボーナス除く)：'):
            amount_invested = line[len('運用開始額(ボーナス除く)：'):].strip()
            amount_invested = amount_invested.replace(',', '')
            result = re.findall(r"[\d,.]+", amount_invested)
            gssheet_data_list.append(float(result[0]))
        if line.startswith('初期Bonus：'):
            initial_bonus = line[len('初期Bonus：'):].strip()
            initial_bonus = initial_bonus.replace(',', '')
            gssheet_data_list.append(float(initial_bonus))
        if line.startswith('現在の残高：'):
            amount_invested = line[len('現在の残高：'):].strip()
            amount_invested = amount_invested.replace(',', '')
            result = re.findall(r"[\d,.]+", amount_invested)
            gssheet_data_list.append(float(result[0]))
        if line.startswith('有効証拠金：'):
            amount_invested = line[len('有効証拠金：'):].strip()
            amount_invested = amount_invested.replace(',', '')
            result = re.findall(r"[\d,.]+", amount_invested)
            gssheet_data_list.append(float(result[0]))
        if line.startswith('含み損益：'):
            unrealized_loss = line[len('含み損益：'):].strip()
            unrealized_loss = unrealized_loss.replace(',', '')
            line_list = re.split(r'円|ドル|USC', unrealized_loss)
            if len(line_list) < 2:
                print("text : ", unrealized_loss)
                continue
            gssheet_data_list.append(extract_number(line_list[0]))
            gssheet_data_list.append(extract_number(line_list[1][1:-2]))
        if line.startswith('収益：'):
            unrealized_loss = line[len('収益：'):].strip()
            unrealized_loss = unrealized_loss.replace(',', '')
            line_list = re.split(r'円|ドル|USC', unrealized_loss)
            if len(line_list) < 2:
                print("text : ", unrealized_loss)
                continue
            gssheet_data_list.append(extract_number(line_list[0]))
            gssheet_data_list.append(extract_number(line_list[1][1:-2]))
        if line.startswith('総収益：'):
            unrealized_loss = line[len('総収益：'):].strip()
            unrealized_loss = unrealized_loss.replace(',', '')
            line_list = re.split(r'円|ドル|USC', unrealized_loss)
            if len(line_list) < 2:
                print("text : ", unrealized_loss)
                continue
            gssheet_data_list.append(extract_number(line_list[0]))
            gssheet_data_list.append(extract_number(line_list[1][1:-2]))
        if line.startswith('最大含み損：'):
            unrealized_loss = line[len('最大含み損：'):].strip()
            unrealized_loss = unrealized_loss.replace(',', '')
            result = re.findall(r"[\d,.]+", unrealized_loss)
            for num in result:
                gssheet_data_list.append(float(num))
        if line.startswith('入金：'):
            unrealized_loss = line[len('入金：'):].strip()
            unrealized_loss = unrealized_loss.replace(',', '')
            result = re.findall(r"[\d,.]+", unrealized_loss)
            for num in result:
                gssheet_data_list.append(float(num))
        if line.startswith('出金：'):
            unrealized_loss = line[len('出金：'):].strip()
            unrealized_loss = unrealized_loss.replace(',', '')
            result = re.findall(r"[\d,.]+", unrealized_loss)
            for num in result:
                gssheet_data_list.append(float(num))
        if line.startswith('クレジット：'):
            amount_invested = line[len('クレジット：'):].strip()
            amount_invested = amount_invested.replace(',', '')
            result = re.findall(r"[\d,.]+", amount_invested)
            gssheet_data_list.append(float(result[0]))

        if "円" in  line:
            currency = "円"
        if "USC" in  line:
            currency = "USC"
        if "ドル" in  line:
            currency = "ドル"

    # currencyをリストに追加
    gssheet_data_list.append(currency)
    q.put(gssheet_data_list)
    print(gssheet_data_list)


    while not q.empty():
        if write_counter < 60:
            message = q.get()
            worksheet.append_row(message)
            print("file out put : ", message)
            write_counter += 1
        else:
            print("Rate limit reached, waiting for 70 seconds")
            queue_size = q.qsize()
            print("残りのキューのサイズ:", queue_size)
            await asyncio.sleep(70)
            write_counter = 0
    else:
        print("q is empty")


bot.run(DISCORD_TOKEN)
