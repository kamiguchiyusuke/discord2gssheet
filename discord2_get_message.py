import discord

# Discord　settings
token = 'token'
intents = discord.Intents.default()
intents.message_content = True
client = discord.Client(intents=intents)

@client.event
async def on_ready():
    print(f'We have logged in as {client.user}')

@client.event
async def on_message(message):
    print("--------------------- on_message ---------------------")
    print(message.content)

client.run(token)
